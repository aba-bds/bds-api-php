<?php

namespace AbaBds\http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use AbaBds\AbaBdsApiException;

/**
 * An HTTP client to request BDS API using Guzzle.
 */
class AbaBdsGuzzleHttpClient implements AbaBdsHttpClientInterface {

  /**
   * The GuzzleHttp client.
   *
   * @var \GuzzleHttp\Client
   */
  private $guzzle;

  /**
   * AbaBdsGuzzleHttpClient constructor.
   *
   * @param array $config
   *   Guzzle HTTP configuration options.
   */
  public function __construct(array $config = []) {
    $this->guzzle = new Client($config);
  }

  /**
   * {@inheritdoc}
   */
  public function handleRequest($method, $uri = '', $options = [], $parameters = [], $returnAssoc = FALSE) {
    if (!empty($parameters)) {
      if ($method == 'GET') {
        // Send parameters as query string parameters.
        $options['query'] = $parameters;
      }
      else {
        // Send parameters as JSON in request body.
        $options['json'] = (object) $parameters;
      }
    }

    try {
      $response = $this->guzzle->request($method, $uri, $options);
      $data = json_decode($response->getBody(), $returnAssoc);
      return $data;
    }
    catch (RequestException $e) {
      $response = $e->getResponse();
      if (!empty($response)) {
        $message = $e->getResponse()->getBody();
      }
      else {
        $message = $e->getMessage();
      }

      throw new AbaBdsApiException($message, $e->getCode(), $e);
    }
  }

}
