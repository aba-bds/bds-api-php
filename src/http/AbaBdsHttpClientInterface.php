<?php

namespace AbaBds\http;

/**
 * Interface for HTTP clients used to contact BDS.
 */
interface AbaBdsHttpClientInterface {

  /**
   * Makes a request to the BDS API.
   *
   * @param string $method
   *   The REST method to use when making the request.
   * @param string $uri
   *   The API URI to request.
   * @param array $options
   *   Request options.
   * @param array $parameters
   *   Associative array of parameters to send in the request body.
   * @param bool $returnAssoc
   *   TRUE to return BDS response as an associative array.
   *
   * @return object
   *   Response object.
   *
   * @throws \Exception
   */
  public function handleRequest(string $method, string $uri = '', array $options = [], array $parameters = [], bool $returnAssoc = FALSE);

}
