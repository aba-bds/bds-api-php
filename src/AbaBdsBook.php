<?php

namespace AbaBds;

/**
 * BDS Book library.
 */
class AbaBdsBook extends AbaBdsApi {

  /**
   * Gets information about a book from BDS API.
   *
   * @param string $isbn
   *   The book's ISBN.
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *   The book information.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#get-a-book
   */
  public function getBook(string $isbn, array $parameters = []) {
    $tokens = [
      'isbn' => $isbn,
    ];

    return $this->request('GET', '/book/{isbn}', $tokens, $parameters);
  }

  /**
   * Add a new book to the BDS.
   *
   * @param string $isbn
   *   The book's ISBN.
   * @param array $parameters
   *   The book metadata.
   *
   * @return object
   *   The new book record.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#add-a-book
   */
  public function addBook(string $isbn, array $parameters = []) {
    $tokens = [
      'isbn' => $isbn,
    ];

    return $this->request('POST', '/book/{isbn}', $tokens, $parameters);
  }

  /**
   * Update an existing book in the BDS.
   *
   * @param string $isbn
   *   The book's ISBN.
   * @param array $parameters
   *   The metadata to update.
   *
   * @return object
   *   The updated book record.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#update-a-book
   */
  public function updateBook(string $isbn, array $parameters = []) {
    $tokens = [
      'isbn' => $isbn,
    ];

    return $this->request('PATCH', '/book/{isbn}', $tokens, $parameters);
  }

  /**
   * Gets information about a book family from BDS API.
   *
   * @param string $family_id
   *   The book family's unique identifier.
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *   The book family information.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#get-related-editions
   */
  public function getFamily(string $family_id, array $parameters = []) {
    $tokens = [
      'family_id' => $family_id,
    ];

    return $this->request('GET', '/family/{family_id}', $tokens, $parameters);
  }

  /**
   * Retrieves all books in a series based on the name of the series.
   *
   * @param string $series
   *   The name of the series.
   * @param string $type
   *   Either 'related', 'numbered' or 'numbered_full'.
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *   A list of books in the series is returned. The exact nature of the
   *   response depends on the type parameter.
   *
   * @see https://bds.booksense.com/docs/#series
   */
  public function getSeries(string $series, string $type, array $parameters = []) {
    $parameters += [
      'series' => $series,
      'type' => $type,
    ];

    return $this->request('GET', '/series', [], $parameters);
  }

  /**
   * Performs a search against BDS API.
   *
   * @param string $q
   *   The search term.
   * @param int $page
   *   The current results page.
   * @param int $per_page
   *   The number of results per page. Max is 100.
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *   The search results object.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#search-for-a-book
   */
  public function search(string $q, int $page = 1, int $per_page = 10, array $parameters = []) {
    $parameters += [
      'q' => $q,
      'page' => $page,
      'per_page' => $per_page,
      'type' => 'keyword',
    ];

    return $this->request('GET', '/search', [], $parameters);
  }

  /**
   * Performs a book search by BISAC code against BDS API.
   *
   * @param array $bisac_codes
   *   An array of BISAC codes to look for.
   * @param int $page
   *   The current results page.
   * @param int $per_page
   *   The number of results per page. Max is 100.
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *   The search results object.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#search-for-a-book
   */
  public function searchByBisacCode(array $bisac_codes, int $page = 1, int $per_page = 10, array $parameters = []) {
    $parameters += [
      'q' => implode(',', $bisac_codes),
      'page' => $page,
      'per_page' => $per_page,
      'type' => 'bisac',
    ];

    return $this->request('GET', '/search', [], $parameters);
  }

  /**
   * Performs a search by author name against BDS API.
   *
   * @param string $author
   *   The name of the author, in "Lastname, First name" format.
   * @param int $page
   *   The current results page.
   * @param int $per_page
   *   The number of results per page. Max is 100.
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *   The search results object.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#search-for-a-book
   */
  public function searchByAuthor(string $author, int $page = 1, int $per_page = 10, array $parameters = []) {
    $parameters += [
      'q' => $author,
      'page' => $page,
      'per_page' => $per_page,
      'type' => 'author',
    ];

    return $this->request('GET', '/search', [], $parameters);
  }

  /**
   * Gets information about an IndieNext list from BDS API.
   *
   * @param string $list
   *   The IndieNext list to retrieve.
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *   The IndieNext list information.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#indie-next-list
   */
  public function getIndieNextList(string $list, array $parameters = []) {
    $tokens = [
      'issue_id' => $list,
    ];

    return $this->request('GET', '/nextlist/{issue_id}.json', $tokens, $parameters);
  }

  /**
   * Gets information about Bestsellers list from BDS API.
   *
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *   The Bestsellers list information.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#indie-bestsellers
   */
  public function getBestsellersList(array $parameters = []) {
    return $this->request('GET', '/bestsellers.json', [], $parameters);
  }

  /**
   * Gets information about a bisac code from BDS API.
   *
   * @param string $bisac_code
   *   The BISAC code to retrieve.
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *   The BISAC code information.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#bisac-codes
   */
  public function getBisacCode(string $bisac_code, array $parameters = []) {
    $tokens = [
      'code' => $bisac_code,
    ];

    return $this->request('GET', '/bisac/{code}', $tokens, $parameters);
  }

  /**
   * Gets information about all root BISAC codes from BDS API.
   *
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return array
   *   Information about all root BISAC codes.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#bisac-codes
   */
  public function getRootBisacCodes(array $parameters = []) {
    return $this->getBisacCode('root', $parameters);
  }

  /**
   * Gets metadata information of a list of books.
   *
   * @param array $isbns
   *   List of ISBN of books to retrieve metadata of.
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *   List of metadata information of requested books.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#metadata
   */
  public function getBookMetadata(array $isbns = [], array $parameters = []) {
    $parameters += [
      'isbns' => $isbns,
    ];

    return $this->request('POST', '/metadata_changed', [], $parameters);
  }

  /**
   * Gets information about stock from BDS API.
   *
   * @param array $isbns
   *   List of ISBN of books to get stock from.
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *   The stock information.
   *
   * @throws AbaBdsApiException
   *
   * @see https://bds.booksense.com/docs/#stock
   */
  public function getStock(array $isbns, array $parameters = []) {
    $tokens = [
      'isbns' => implode(',', $isbns),
    ];
    return $this->request('GET', '/stock/{isbns}', $tokens, $parameters);
  }

}
