<?php

namespace AbaBds;

use AbaBds\http\AbaBdsCurlHttpClient;
use AbaBds\http\AbaBdsGuzzleHttpClient;
use AbaBds\http\AbaBdsHttpClientInterface;

/**
 * Library to interact with ABA's Books Data System API.
 */
class AbaBdsApi {

  /**
   * The HTTP client.
   *
   * @var \AbaBds\http\AbaBdsHttpClientInterface
   */
  private $client;

  /**
   * The REST API endpoint.
   *
   * @var string
   */
  private $endpoint;

  /**
   * The BDS API username to authenticate with.
   *
   * @var string
   */
  private $apiUser;

  /**
   * The BDS API password to authenticate with.
   *
   * @var string
   */
  private $apiPass;

  /**
   * The BDS JWT token to authenticate with.
   *
   * @var object
   */
  private $apiToken;

  /**
   * AbaBdsApi constructor.
   *
   * @param string $api_user
   *   The BDS API username.
   * @param string $api_pass
   *   The BDS API pass.
   * @param string $endpoint
   *   Endpoint to send requests to.
   */
  public function __construct(string $api_user = '', string $api_pass = '', string $endpoint = '') {
    $this->apiUser = $api_user;
    $this->apiPass = $api_pass;
    $this->endpoint = $endpoint ?: '';
    $this->client = $this->getDefaultHttpClient();
  }

  /**
   * Sets an endpoint to execute requests against.
   *
   * @param string $endpoint
   *   Endpoint to send requests to.
   */
  public function setEndpoint(string $endpoint) {
    $this->endpoint = $endpoint;
  }

  /**
   * Sets a custom HTTP client to be used for all API requests.
   *
   * @param AbaBds\http\AbaBdsHttpClientInterface $client
   *   The HTTP client.
   */
  public function setClient(AbaBdsHttpClientInterface $client) {
    $this->client = $client;
  }

  /**
   * Sets a JWT token to be used for all API requests.
   *
   * @param object $token
   *   The JWT token. Required properties are token and expires_at.
   */
  public function setToken($token) {
    if (
      isset($token->token)
      && is_string($token->token)
      && isset($token->expires_at)
      && is_numeric($token->expires_at)
    ) {
      $this->apiToken = $token;
    }
  }

  /**
   * Makes a request to the BDS API.
   *
   * @param string $method
   *   The REST method to use when making the request.
   * @param string $path
   *   The API path to request.
   * @param array $tokens
   *   Associative array of tokens and values to replace in the path.
   * @param array $parameters
   *   Associative array of parameters to send in the request body.
   * @param bool $returnAssoc
   *   TRUE to return BDS API response as an associative array.
   *
   * @return mixed
   *   Object or Array if $returnAssoc is TRUE.
   *
   * @throws AbaBdsApiException
   */
  public function request(string $method, string $path, array $tokens = [], array $parameters = [], bool $returnAssoc = FALSE) {
    // Process in-url tokens.
    if (!empty($tokens)) {
      foreach ($tokens as $key => $value) {
        $path = str_replace('{' . $key . '}', $value, $path);
      }
    }

    // Authorization header.
    $options = [
      'headers' => [
        'Authorization' => sprintf('Token %s', $this->getJsonWebToken()->token),
      ],
    ];

    return $this->client->handleRequest($method, $this->endpoint . $path, $options, $parameters, $returnAssoc);
  }

  /**
   * Returns a Json Web Token to consume the API.
   *
   * @return object
   *   The Json Web Token.
   *
   * @throws AbaBdsApiException
   */
  public function getJsonWebToken() {
    if ($this->apiToken && ($this->apiToken->expires_at > time())) {
      return $this->apiToken;
    }

    try {
      $options = [
        'headers' => [
          'Authorization' => sprintf('Basic %s', base64_encode(sprintf('%s:%s', $this->apiUser, $this->apiPass))),
        ],
      ];
      $this->apiToken = $this->client->handleRequest('POST', $this->endpoint . '/authorize', $options);
    }
    catch (AbaBdsApiException $e) {
      throw $e;
    }

    return $this->apiToken;
  }

  /**
   * Instantiates a default HTTP client based on the local environment.
   *
   * @return AbaBdsHttpClientInterface
   *   The HTTP client.
   */
  private function getDefaultHttpClient() {
    $client = NULL;

    // Use cURL HTTP client if PHP version is below 5.5.0.
    // Use Guzzle client otherwise.
    if (version_compare(phpversion(), '5.5.0', '<')) {
      $client = new AbaBdsCurlHttpClient();
    }
    else {
      $client = new AbaBdsGuzzleHttpClient();
    }

    return $client;
  }

}
