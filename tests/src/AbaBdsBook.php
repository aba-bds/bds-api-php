<?php

namespace AbaBds\Tests;

/**
 * Aba BDS Book library for testing.
 *
 * @package AbaBds\Tests
 */
class AbaBdsBook extends \AbaBds\AbaBdsBook {

  /**
   * {@inheritdoc}
   */
  public function __construct(string $api_user = '', string $api_pass = '', string $endpoint = '') {
    $this->client = new AbaBdsTestHttpClient();
    $this->endpoint = $endpoint;
  }

  /**
   * {@inheritdoc}
   */
  public function getJsonWebToken() {
    return (object) [
      'token' => '123',
      'expires_at' => '123',
    ];
  }

}
