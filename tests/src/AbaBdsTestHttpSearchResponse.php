<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for search endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpSearchResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      'page' => 1,
      'per_page' => 10,
      'total_pages' => 104,
      'total_results' => 1036,
      'results' => [
        [
          'id' => 90824,
          'type' => 'ababook',
          'title' => 'Moby-Dick: or, The Whale',
          'isbn' => '9781982144401',
        ],
        [
          'id' => 23000,
          'type' => 'ababook',
          'title' => 'Moby Dick (Harper Perennial Deluxe Editions)',
          'isbn' => '9780062085641',
        ],
      ],
    ];

    return json_encode($response);
  }

}
