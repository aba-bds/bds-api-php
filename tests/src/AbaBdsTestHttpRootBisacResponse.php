<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for root bisac codes endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpRootBisacResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      'root' => [
        'name' => 'Browse Books',
        'short_name' => 'Browse Books',
        'code' => 'root',
        'ancestor' => '',
        'children' => [
          [
            'code' => 'ANT000000',
            'name' => 'Antiques & Collectibles',
            'short_name' => 'Antiques & Collectibles',
            'ancestor' => '',
          ],
          [
            'code' => 'ARC000000',
            'name' => 'Architecture',
            'short_name' => 'Architecture',
            'ancestor' => '',
          ],
        ],
      ],
    ];

    return json_encode($response);
  }

}
