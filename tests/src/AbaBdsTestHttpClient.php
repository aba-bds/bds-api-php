<?php

namespace AbaBds\Tests;

use AbaBds\http\AbaBdsHttpClientInterface;

/**
 * A dummy HTTP client used when running unit tests.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpClient implements AbaBdsHttpClientInterface {

  public $method;

  public $uri;

  public $options;

  /**
   * {@inheritdoc}
   */
  public function handleRequest($method, $uri = '', $options = [], $parameters = [], $returnAssoc = FALSE) {
    $this->method = $method;
    $this->uri = $uri;
    $this->options = $options;

    // Default response.
    $response = new AbaBdsTestHttpResponse();

    // Responses for specific cases.
    if ($uri === 'https://test.endpoint.dev/book/9781982144401') {
      $response = new AbaBdsTestHttpBookResponse();
    }
    if ($uri === 'https://test.endpoint.dev/book/9781234567890' && $method === 'POST') {
      $response = new AbaBdsTestHttpAddBookResponse();
    }
    if ($uri === 'https://test.endpoint.dev/book/9781234567890' && $method === 'PATCH') {
      $response = new AbaBdsTestHttpUpdateBookResponse();
    }
    if ($uri === 'https://test.endpoint.dev/family/0000810745') {
      $response = new AbaBdsTestHttpFamilyResponse();
    }
    if ($uri === 'https://test.endpoint.dev/bisac/FIC009000') {
      $response = new AbaBdsTestHttpBisacResponse();
    }
    if ($uri === 'https://test.endpoint.dev/bisac/root') {
      $response = new AbaBdsTestHttpRootBisacResponse();
    }
    if (
      $uri === 'https://test.endpoint.dev/search' &&
      isset($parameters['q']) &&
      $parameters['q'] === 'Moby Dick'
    ) {
      $response = new AbaBdsTestHttpSearchResponse();
    }
    if (
      $uri === 'https://test.endpoint.dev/search' &&
      isset($parameters['q']) &&
      $parameters['q'] === 'FIC009' &&
      isset($parameters['type']) &&
      $parameters['type'] === 'bisac'
    ) {
      $response = new AbaBdsTestHttpSearchBisacResponse();
    }
    if (
      $uri === 'https://test.endpoint.dev/search' &&
      isset($parameters['q']) &&
      $parameters['q'] === 'Rice, Anne' &&
      isset($parameters['type']) &&
      $parameters['type'] === 'author'
    ) {
      $response = new AbaBdsTestHttpSearchAuthorResponse();
    }
    if ($uri === 'https://test.endpoint.dev/nextlist/next.json') {
      $response = new AbaBdsTestHttpIndieNextResponse();
    }
    if ($uri === 'https://test.endpoint.dev/bestsellers.json') {
      $response = new AbaBdsTestHttpBestsellersResponse();
    }
    if ($uri === 'https://test.endpoint.dev/metadata_changed' && $method === 'POST') {
      $response = new AbaBdsTestHttpMetadataResponse();
    }
    if ($uri === 'https://test.endpoint.dev/stock/9780374157357,9780593230572') {
      $response = new AbaBdsTestHttpStockResponse();
    }
    if (
      $uri === 'https://test.endpoint.dev/series' &&
      isset($parameters['series']) &&
      $parameters['series'] === 'Chronicles of Narnia' &&
      isset($parameters['type']) &&
      $parameters['type'] === 'related'
    ) {
      $response = new AbaBdsTestHttpSeriesResponse();
    }


    return json_decode($response->getBody(), $returnAssoc);
  }

}
