<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for book endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpUpdateBookResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      'isbn' => '9781234567890',
      'title' => 'My Updated Book',
      'active' => 1,
      'list_price' => 14.99,
      'binding_type' => 'Paperback',
      'publisher_imprint' => 'Testy Books',
      'publication_date' => '2022-03-08',
      'contributors' => [
        [
          'name' => 'Author, Sample',
        ],
      ],
    ];

    return json_encode($response);
  }

}
