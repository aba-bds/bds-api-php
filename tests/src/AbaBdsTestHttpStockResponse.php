<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for stock endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpStockResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      '9780374157357' => [
        'ingram_stock' => [
          'tn_on_hand' => 66,
          'pa_on_hand' => 146,
          'in_on_hand' => 282,
          'or_on_hand' => 915,
          'pn_on_hand' => 0,
          'tj_on_hand' => 0,
          'tn_on_order' => 120,
          'pa_on_order' => 483,
          'in_on_order' => 0,
          'or_on_order' => 8,
          'pn_on_order' => 0,
          'tj_on_order' => 0,
          'ingram_api' => true,
          'last_processed' => '2022-04-05 14:06:21',
          'ingram_discount_code' => 'REG',
          'ingram_price' => '35.00',
          'returnable' => true,
          'media_mail' => true
        ],
        'bookazine_stock' => [
          'bookazine_on_hand' => 227,
          'bookazine_on_order' => 0,
          'bookazine_price' => '35.00',
          'bookazine_discount_code' => null,
          'last_processed' => '2022-04-05 07:29:18'
        ]
      ],
      '9780593230572' =>       [
        'ingram_stock' => [
          'tn_on_hand' => 4298,
          'pa_on_hand' => 1499,
          'in_on_hand' => 1585,
          'or_on_hand' => 4445,
          'pn_on_hand' => 0,
          'tj_on_hand' => 0,
          'tn_on_order' => 0,
          'pa_on_order' => 0,
          'in_on_order' => 0,
          'or_on_order' => 0,
          'pn_on_order' => 0,
          'tj_on_order' => 0,
          'ingram_api' => true,
          'last_processed' => '2022-04-05 14:06:21',
          'ingram_discount_code' => 'REG',
          'ingram_price' => '38.00',
          'returnable' => true,
          'media_mail' => true
        ],
        'bookazine_stock' => [
          'bookazine_on_hand' => 179,
          'bookazine_on_order' => 0,
          'bookazine_price' => '38.00',
          'bookazine_discount_code' => null,
          'last_processed' => '2022-04-05 07:29:18'
        ]
      ]
    ];

    return json_encode($response);
  }

}
