<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for family endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpFamilyResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      [
        'label' => 'The Lord of the Rings',
        'model' => '9780618640157',
        'desire' => 794,
        'abridged' => NULL,
        'large_print' => NULL,
        'publication_date' => '2005-10-12T04:00:00.284Z',
        'binding_type' => 'Paperback',
        'language' => 'English',
        'type' => 'ababook',
        'ingram_stock' => 460,
      ],
      [
        'label' => 'The Lord of the Rings: 50th Anniversary Edition',
        'model' => '9780618517657',
        'desire' => 568,
        'abridged' => NULL,
        'large_print' => NULL,
        'publication_date' => '2004-10-21T04:00:00.294Z',
        'binding_type' => 'Hardcover',
        'language' => 'English',
        'type' => 'ababook',
        'ingram_stock' => 196,
      ],
    ];

    return json_encode($response);
  }

}
