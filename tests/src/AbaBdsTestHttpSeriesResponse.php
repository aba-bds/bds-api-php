<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for series endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpSeriesResponse extends AbaBdsTestHttpResponse {

  /**
   * {@inheritdoc}
   */
  public function getBody() {
    $response = [
      [
        'isbn' => '9780064404990',
        'title' => 'The Lion, the Witch and the Wardrobe (Chronicles of Narnia #2)',
        'series' => 'Chronicles of Narnia',
        'binding_type' => 'Paperback',
        'bds_score' => 172,
        'type' => 'ababook',
        'publication_date' => '2008-01-02T05:00:00.100Z',
        'author' => 'Lewis, C. S.',
        'series_number' => '2',
        'cover_image_large' => 'https://images.booksense.com/images/990/404/9780064404990.jpg',
        'cover_image_small' => 'https://images.booksense.com/images/books/990/404/FC9780064404990.JPG'
      ],
      [
        'isbn' => '9780064405058',
        'title' => "The Magician's Nephew (Chronicles of Narnia #1)",
        'series' => 'Chronicles of Narnia',
        'binding_type' => 'Paperback',
        'bds_score' => 103,
        'type' => 'ababook',
        'publication_date' => '2008-01-02T05:00:00.100Z',
        'author' => 'Lewis, C. S.',
        'series_number' => '1',
        'cover_image_large' => 'https://images.booksense.com/images/058/405/9780064405058.jpg',
        'cover_image_small' => 'https://images.booksense.com/images/books/058/405/FC9780064405058.JPG'
      ],
    ];

    return json_encode($response);
  }

}
