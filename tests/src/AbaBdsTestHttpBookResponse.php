<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for book endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpBookResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      'title' => 'Damnation Spring',
      'isbn' => '9781982144401',
      'weight' => 1.69,
      'length' => 9,
      'width' => 6,
      'height' => 1.3,
      'list_price' => 28,
    ];

    return json_encode($response);
  }

}
