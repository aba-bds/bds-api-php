<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for metadata endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpMetadataResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      '9780345325815' => 1584306011,
      '9780544445789' => 1641809100,
    ];

    return json_encode($response);
  }

}
