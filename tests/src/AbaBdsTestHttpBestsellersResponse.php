<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for Bestsellers list endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpBestsellersResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      'title' => 'Indie Bestsellers for February 2nd, 2022',
      'description' => 'For the week ending January 30th, 2022, based on sales ...',
      'for_date' => '2022-02-02',
      'end_date' => '2022-01-30',
      'sections' => [
        [
          'title' => 'Hardcover Fiction Bestsellers',
          'entries' => [
            [
              'isbn' => '9780735222359',
              'title' => 'The Lincoln Highway',
              'author' => 'Amor Towles',
              'publisher' => 'Viking',
              'description' => 'Towles stylish and propulsive novel set in 1950s America is an October 2021 Indie Next Pick.',
              'small_image_uri' => 'https://images.booksense.com/images/books/359/222/FC9780735222359.JPG',
              'large_image_uri' => 'https://images.booksense.com/images/359/222/9780735222359.jpg',
              'rank' => '1',
              'last' => '1',
              'weeks_on_list' => '17'
            ],
          ],
        ],
        [
          'title' => 'Hardcover Nonfiction Bestsellers',
          'entries' => [
            [
              'isbn' => '9780399592553',
              'title' => 'Atlas of the Heart: Mapping Meaningful Connection and the Language of Human Experience',
              'author' => 'Brene Brown',
              'publisher' => 'Random House',
              'description' => 'Brown takes us on a journey through 87 of the emotions and experiences that define what it means to be human.',
              'small_image_uri' => 'https://images.booksense.com/images/books/553/592/FC9780399592553.JPG',
              'large_image_uri' => 'https://images.booksense.com/images/553/592/9780399592553.jpg',
              'rank' => '1',
              'last' => '1',
              'weeks_on_list' => '9'
            ],
          ],
        ],
      ],
    ];

    return json_encode($response);
  }

}
