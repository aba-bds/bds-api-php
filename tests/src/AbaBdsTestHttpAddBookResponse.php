<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for book endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpAddBookResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      'isbn' => '9781234567890',
      'title' => 'My Test Book',
      'active' => 1,
      'list_price' => 19.99,
      'binding_type' => 'Paperback',
      'publisher_imprint' => 'Testy Books',
      'publication_date' => '2022-03-08',
      'contributors' => [
        [
          'name' => 'Author, Sample',
        ],
      ],
    ];

    return json_encode($response);
  }

}
