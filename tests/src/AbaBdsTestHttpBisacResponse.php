<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for bisac code endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpBisacResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      'FIC009000' => [
        'code' => 'FIC009000',
        'name' => 'Fiction / Fantasy',
        'short_name' => 'Fantasy',
        'ancestor' => 'FIC000000',
        'ancestors' => [
          [
            'code' => 'FIC000000',
            'name' => 'Fiction',
            'short_name' => 'Fiction',
            'ancestor' => 'root',
          ],
        ],
        'children' => [
          [
            'code' => 'FIC009100',
            'name' => 'Fiction / Fantasy / Action & Adventure',
            'short_name' => 'Action & Adventure',
            'ancestor' => 'FIC009000',
          ],
          [
            'code' => 'FIC009110',
            'name' => 'Fiction / Fantasy / Arthurian',
            'short_name' => 'Arthurian',
            'ancestor' => 'FIC009000',
          ],
        ],
      ],
    ];

    return json_encode($response);
  }

}
