<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for IndieNext list endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpIndieNextResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      'issue_id' => '202202',
      'title' => 'February 2022 Indie Next List',
      'description' => 'The Indie Next List, drawn from ...',
      'sections' => [
        [
          'title' => 'Great Reads',
          'entries' => [
            [
              'isbn' => '9780593419335',
              'title' => 'Love & Saffron: A Novel of Friendship, Food, and Love',
              'author' => 'Kim Fay',
              'format' => 'Hardcover',
              'publisher' => 'G.P. Putnams Sons',
              'pubdate' => '2022-02-08',
              'price' => '24.00',
              'blurb' => '<p>“<em>Love &amp; Saffron</em>&nbsp;reminds us of the beauty of letter writing. I loved the simplicity of this book — it filled my heart with love and connection to the human spirit, and left me with the desire to kindle a friendship by sending a letter.”</p> ',
              'bookseller' => 'Annette Avery',
              'bookstore' => 'Bright Side Bookshop',
              'bookstore_city' => 'Flagstaff',
              'bookstore_state' => 'AZ',
              'rank' => 1
            ],
          ],
        ],
        [
          'title' => 'Now in Paperback',
          'entries' => [
            [
              'isbn' => '9780063072640',
              'title' => 'How High We Go in the Dark: A Novel',
              'author' => 'Sequoia Nagamatsu',
              'format' => 'Hardcover',
              'publisher' => 'William Morrow',
              'pubdate' => '2022-01-18',
              'price' => '27.99',
              'blurb' => '<p>“This collection of connected stories describes the world after a devastating plague. The book is beautiful — striking, unsettling, and darkly gorgeous. It defies categorization and creates its own genre. A shimmering gem of a book.”<br /> &nbsp;</p> ',
              'bookseller' => 'Debra Ginsberg',
              'bookstore' => 'DIESEL, A Bookstore',
              'bookstore_city' => 'Santa Monica',
              'bookstore_state' => 'CA',
              'rank' => 2
            ],
          ],
        ],
      ],
    ];

    return json_encode($response);
  }

}
