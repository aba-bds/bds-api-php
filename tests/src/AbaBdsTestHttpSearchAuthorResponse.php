<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for search by author endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpSearchAuthorResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      'page' => 1,
      'per_page' => 10,
      'total_pages' => 6979,
      'total_results' => 69783,
      'results' => [
        [
          'id' => 269002,
          'type' => 'ababook',
          'title' => 'Interview with the Vampire (Vampire Chronicles)',
          'isbn' => '9780345337665',
        ],
        [
          'id' => 19331433,
          'type' => 'ababook',
          'title' => 'These Precious Days: Essays',
          'isbn' => '9780063092785',
        ],
      ],
    ];

    return json_encode($response);
  }

}
