<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response for search by BISAC endpoint.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpSearchBisacResponse extends AbaBdsTestHttpResponse {

  public function getBody() {
    $response = [
      'page' => 1,
      'per_page' => 10,
      'total_pages' => 19914,
      'total_results' => 199137,
      'results' => [
        [
          'id' => 20170499,
          'type' => 'ababook',
          'title' => 'The Witchlight Carnival Dice & Miscellany',
          'isbn' => '9780786967216',
          'bisac_codes' => [
            [
              'code' => 'GAM010000',
              'name' => 'Games & Activities / Role Playing & Fantasy',
              'book_id' => NULL,
            ],
            [
              'code' => 'FIC009120',
              'name' => 'Fiction / Fantasy / Dragons & Mythical Creatures',
              'book_id' => NULL,
            ],
          ],
        ],
        [
          'id' => 336527,
          'type' => 'ababook',
          'title' => 'Dune',
          'isbn' => '9780441013593',
          'bisac_codes' => [
            [
              'code' => 'FIC004000',
              'name' => 'Fiction / Classics',
              'book_id' => NULL,
            ],
            [
              'code' => 'FIC028030',
              'name' => 'Fiction / Science Fiction / Space Opera',
              'book_id' => NULL,
            ],
            [
              'code' => 'FIC009020',
              'name' => 'Fiction / Fantasy / Epic',
              'book_id' => NULL,
            ],
          ],
        ],
      ],
    ];

    return json_encode($response);
  }

}
