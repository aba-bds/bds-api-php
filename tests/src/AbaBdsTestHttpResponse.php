<?php

namespace AbaBds\Tests;

/**
 * Test HTTP Response.
 *
 * @package AbaBds\Tests
 */
class AbaBdsTestHttpResponse {

  public function getBody() {
    return '{}';
  }

}
