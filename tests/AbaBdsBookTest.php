<?php

namespace AbaBds\Tests;

use PHPUnit\Framework\TestCase;

/**
 * Aba BDS Book library test cases.
 *
 * @package AbaBds\Tests
 */
class AbaBdsBookTest extends TestCase {

  /**
   * Tests library functionality for book information retrieval.
   */
  public function testGetBook() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $book = $mc->getBook('9781982144401');

    $this->assertEquals('Damnation Spring', $book->title);
    $this->assertEquals('9781982144401', $book->isbn);
    $this->assertEquals(28, $book->list_price);
  }

  /**
   * Tests library functionality for adding a book.
   */
  public function testAddBook() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $book_params = [
      'title' => 'My Test Book',
      'active' => 1,
      'list_price' => 19.99,
      'binding_type' => 'Paperback',
      'publisher_imprint' => 'Testy Books',
      'publication_date' => '2022-03-08',
      'contributors' => [
        [
          'name' => 'Author, Sample',
        ],
      ],
    ];

    $book = $mc->addBook('9781234567890', $book_params);

    $this->assertEquals('My Test Book', $book->title);
    $this->assertEquals('9781234567890', $book->isbn);
    $this->assertEquals(19.99, $book->list_price);
  }

  /**
   * Tests library functionality for updating a book.
   */
  public function testUpdateBook() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $book_params = [
      'title' => 'My Updated Book',
      'list_price' => 14.99,
    ];

    $book = $mc->updateBook('9781234567890', $book_params);

    $this->assertEquals('My Updated Book', $book->title);
    $this->assertEquals('9781234567890', $book->isbn);
    $this->assertEquals(14.99, $book->list_price);
  }

  /**
   * Tests library functionality for book family information retrieval.
   */
  public function testGetFamily() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $family = $mc->getFamily('0000810745');

    $this->assertEquals('The Lord of the Rings', $family[0]->label);
    $this->assertEquals('9780618640157', $family[0]->model);
    $this->assertEquals('English', $family[0]->language);
    $this->assertEquals('The Lord of the Rings: 50th Anniversary Edition', $family[1]->label);
    $this->assertEquals('9780618517657', $family[1]->model);
    $this->assertEquals('English', $family[1]->language);
  }

  /**
   * Tests library functionality for bisac code information retrieval.
   */
  public function testGetBisac() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $code = $mc->getBisacCode('FIC009000');
    $bisac = $code->FIC009000;

    $this->assertEquals('FIC009000', $bisac->code);
    $this->assertEquals('Fiction / Fantasy', $bisac->name);
    $this->assertEquals('Fantasy', $bisac->short_name);
    $this->assertEquals('FIC000000', $bisac->ancestor);
  }

  /**
   * Tests library functionality for root bisac codes information retrieval.
   */
  public function testGetRootBisacCodes() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $codes = $mc->getRootBisacCodes();
    $root = $codes->root;

    $this->assertEquals('Browse Books', $root->name);
    $this->assertEquals('Browse Books', $root->short_name);
    $this->assertEquals('root', $root->code);
    $this->assertEquals('ANT000000', $root->children[0]->code);
    $this->assertEquals('Antiques & Collectibles', $root->children[0]->name);
    $this->assertEquals('Antiques & Collectibles', $root->children[0]->short_name);
    $this->assertEquals('ARC000000', $root->children[1]->code);
    $this->assertEquals('Architecture', $root->children[1]->name);
    $this->assertEquals('Architecture', $root->children[1]->short_name);
  }

  /**
   * Tests library functionality for book search feature.
   */
  public function testSearch() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $search = $mc->search('Moby Dick', 1, 10);

    $this->assertEquals(1, $search->page);
    $this->assertEquals(10, $search->per_page);
    $this->assertEquals(104, $search->total_pages);
    $this->assertEquals(1036, $search->total_results);
    $this->assertEquals(90824, $search->results[0]->id);
    $this->assertEquals(23000, $search->results[1]->id);
  }

  /**
   * Tests library functionality for book search by BISAC code feature.
   */
  public function testSearchByBisacCode() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $search = $mc->searchByBisacCode(['FIC009'], 1, 10);

    $this->assertEquals(1, $search->page);
    $this->assertEquals(10, $search->per_page);
    $this->assertEquals(19914, $search->total_pages);
    $this->assertEquals(199137, $search->total_results);
    $this->assertEquals(20170499, $search->results[0]->id);
    $this->assertEquals(336527, $search->results[1]->id);

    array_walk($search->results, function ($result) {
      $codes = array_column($result->bisac_codes, 'code');
      $found = array_reduce($codes, function ($carry, $code) {
        return $carry ?: strpos($code, 'FIC009') === 0;
      }, FALSE);
      $this->assertEquals(TRUE, $found);
    });
  }

  /**
   * Tests library functionality for book search by author feature.
   */
  public function testSearchByAuthor() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $search = $mc->searchByAuthor('Rice, Anne', 1, 10);

    $this->assertEquals(1, $search->page);
    $this->assertEquals(10, $search->per_page);
    $this->assertEquals(6979, $search->total_pages);
    $this->assertEquals(69783, $search->total_results);
    $this->assertEquals(269002, $search->results[0]->id);
    $this->assertEquals(19331433, $search->results[1]->id);
  }
  /**
   * Tests library functionality for IndieNext list feature.
   */
  public function testGetIndieNextList() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $list = $mc->getIndieNextList('next');

    $this->assertEquals(202202, $list->issue_id);
    $this->assertEquals('Great Reads', $list->sections[0]->title);
    $this->assertEquals('9780593419335', $list->sections[0]->entries[0]->isbn);
    $this->assertEquals('Now in Paperback', $list->sections[1]->title);
    $this->assertEquals('9780063072640', $list->sections[1]->entries[0]->isbn);
  }

  /**
   * Tests library functionality for Bestsellers list feature.
   */
  public function testGetBestsellersList() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $list = $mc->getBestsellersList();

    $this->assertEquals('Indie Bestsellers for February 2nd, 2022', $list->title);
    $this->assertEquals('2022-02-02', $list->for_date);
    $this->assertEquals('Hardcover Fiction Bestsellers', $list->sections[0]->title);
    $this->assertEquals('9780735222359', $list->sections[0]->entries[0]->isbn);
    $this->assertEquals('Hardcover Nonfiction Bestsellers', $list->sections[1]->title);
    $this->assertEquals('9780399592553', $list->sections[1]->entries[0]->isbn);
  }

  /**
   * Tests library functionality for book metadata feature.
   */
  public function testGetBookMetadata() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $metadata = $mc->getBookMetadata([
      '9780345325815',
      '9780544445789',
    ]);

    $this->assertEquals(1584306011, $metadata->{"9780345325815"});
    $this->assertEquals(1641809100, $metadata->{"9780544445789"});
  }

  /**
   * Tests library functionality for stock information retrieval.
   */
  public function testGetStock() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $isbns = ['9780374157357', '9780593230572'];
    $stock = $mc->getStock($isbns);

    $this->assertEquals(66, $stock->{'9780374157357'}->ingram_stock->tn_on_hand);
    $this->assertEquals(146, $stock->{'9780374157357'}->ingram_stock->pa_on_hand);
    $this->assertEquals(282, $stock->{'9780374157357'}->ingram_stock->in_on_hand);

    $this->assertEquals(4298, $stock->{'9780593230572'}->ingram_stock->tn_on_hand);
    $this->assertEquals(1499, $stock->{'9780593230572'}->ingram_stock->pa_on_hand);
    $this->assertEquals(1585, $stock->{'9780593230572'}->ingram_stock->in_on_hand);
  }

  /**
   * Tests library functionality for series information retrieval.
   */
  public function testGetSeries() {
    $mc = new AbaBdsBook('user', 'pass');
    $mc->setClient(new AbaBdsTestHttpClient());
    $mc->setEndpoint('https://test.endpoint.dev');

    $series = $mc->getSeries('Chronicles of Narnia', 'related');

    $this->assertEquals('9780064404990', $series[0]->isbn);
    $this->assertEquals('The Lion, the Witch and the Wardrobe (Chronicles of Narnia #2)', $series[0]->title);
    $this->assertEquals(2, $series[0]->series_number);

    $this->assertEquals('9780064405058', $series[1]->isbn);
    $this->assertEquals("The Magician's Nephew (Chronicles of Narnia #1)", $series[1]->title);
    $this->assertEquals(1, $series[1]->series_number);
  }

}
